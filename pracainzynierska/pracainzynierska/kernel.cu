#include <stdio.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <cmath>
#include "cufft.h"
#include <algorithm>
#include <string.h>
#include <fstream>
#include <iostream>
#include <string>
#include <list>
#include <iomanip>

using namespace std;
typedef unsigned long long int uint64;
const int keySizeInBytes = 32 * sizeof(uint64);

__forceinline__ __device__ uint64 sub(uint64 * from, uint64 *what, uint64 *result){
	uint64 carry = 0;
	uint64 base = ~0;
	uint64 res0 = 0;
	uint64 res1 = 0;

	if (from[31] < what[31]){
		res0 = base + 1 + from[31] - what[31];
		carry = 1;
	}

	else{
		res0 = from[31] - what[31];
		carry = 0;
	}

	int shift = (__ffsll(res0)) - 1;

	if (shift < 0)
		shift = 0;

	res0 = res0 >> shift;

#pragma unroll 31
	for (int i = 30; i >= 0; i--){

		if (from[i] < what[i]){
			res1 = base + 1 + from[i] - what[i] - carry;
			result[i + 1] = res0 | (res1 << (64 - shift));
			res0 = res1 >> shift;
			carry = 1;
		}else{
			res1 = from[i] - what[i] - carry;
			result[i + 1] = res0 | (res1 << (64 - shift));
			res0 = res1 >> shift;
			carry = 0;
		}
	}
	result[0] = res0;
}

__forceinline__ __device__ void moveKeysToTempMatrix(uint64 * x, uint64 * y, uint64 *tempX, uint64 * tempY){
#pragma unroll 32
	for (unsigned char i = 0; i < 32; i++){
		tempX[i] = x[i];
		tempY[i] = y[i];
	}
}

__device__ bool bGreaterThanA(uint64* a, uint64 *b){

#pragma unroll 32
	for (int i = 0; i < 32; i++){

		if (b[i] > a[i]){
			return true;
		}else if (b[i] < a[i]){
			return false;
		}
	}
	return false;
}

__device__ void cudaSwap(uint64 ** a, uint64 ** b){

	uint64  *temp = *a;
	*a = *b;
	*b = temp;
}

__forceinline__ __device__ bool isLowerThanHalf(uint64 * a){

#pragma unroll 32
	for (int i = 0; i < 17; i++){
		if (a[i] != 0){
			return false;
		}
	}
	return true;
}

__forceinline__ __device__ void moveKey(uint64 * from, uint64 * to){

#pragma unroll 32
	for (int i = 0; i < 32; i++){
		to[i] = from[i];
	}

}

__global__ void GCD(uint64 *keysX, uint64 *keysY, uint64 *tempX, uint64 *tempY, int offsetX, int offsetY, int tileSize){

	int kernelGlobalX = threadIdx.x + blockIdx.x*blockDim.x;
	int kernelGlobalY = threadIdx.y + blockIdx.y*blockDim.y;
	int globalX = kernelGlobalX + offsetX*tileSize;
	int globalY = kernelGlobalY + offsetY*tileSize;
	int linearIndex = kernelGlobalX + kernelGlobalY * tileSize;

	if (globalY < globalX){
		uint64 *a = keysX + (kernelGlobalX * 32);
		uint64 *b = keysY + (kernelGlobalY * 32);

		if (bGreaterThanA(a, b)){
			cudaSwap(&a, &b); //a on start is always greater than b 
		}

		moveKey(a, tempX + linearIndex * 32);
		moveKey(b, tempY + linearIndex * 32);
		a = tempX + linearIndex * 32;
		b = tempY + linearIndex * 32;

		do{
			sub(a, b, a);//a = a-b

			if (bGreaterThanA(a, b)){
				cudaSwap(&a, &b);
			}

		} while (!isLowerThanHalf(b));


		if ((tempY[linearIndex * 32 + 31] != 0) && (tempX[linearIndex * 32 + 31] == 0)){
			moveKey(tempY + linearIndex * 32, tempX + linearIndex * 32);
		}else if ((tempY[linearIndex * 32 + 31] != 0) && (tempX[linearIndex * 32 + 31] != 0)){
			memset(tempX + linearIndex * 32, 0, 256);
		}
	}
}

string getSingleKeyFromFile(string line){

	line = line.substr(0, 512);
	return line;
}

int charToHex(char c){

	switch (c){
	case '0': return 0;
	case '1': return 1;
	case '2': return 2;
	case '3': return 3;
	case '4': return 4;
	case '5': return 5;
	case '6': return 6;
	case '7': return 7;
	case '8': return 8;
	case '9': return 9;
	case 'A': return 10;
	case 'B': return 11;
	case 'C': return 12;
	case 'D': return 13;
	case 'E': return 14;
	case 'F': return 15;
	}
}

void stringKeyToBin(string charKey, uint64 * key){

	int j = 0;
	for (int i = 0; i < 512; i++){
		j = i / 16;
		key[j] += charToHex(charKey[i]);
		if (i % 16 != 15){
			key[j] = key[j] << 4;
		}
	}
}

void loadKeysFromFile(string filename, uint64 * keys){

	string line;
	string stringKey;
	int offset = 0;
	std::fstream file;
	file.open(filename, std::ios::in);
	if (file.good()){
		int i = 0;
		while (getline(file, line)){
			stringKey = getSingleKeyFromFile(line);
			offset = i * 32;
			stringKeyToBin(stringKey, keys + offset);
			i++;
		}
		file.close();
		cout << "reading done" << endl;
	}else{
		cout << " cannot open file" << endl;
	}
}

int getFileLinesCount(string filename){

	string line;
	std::fstream file;
	file.open(filename, std::ios::in);
	if (file.good() == true){
		int linesCount = 0;
		cout << "file succesfully open" << endl;
		while (getline(file, line)){
			linesCount++;
		}
		file.close();
		return linesCount;
	}else return 0;
}

void CUDA_CHECK(cudaError_t cudaStatus){

	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "ERROR: %s\n", cudaGetErrorString(cudaStatus));
	}
}

void saveResultsToFile(uint64 *unfiltredResults, int tileSize, string outputFile, int xOffset, int yOffset){

	std::fstream file;
	file.open(outputFile, std::ios_base::app | std::ios_base::out);

	if (file.good() == true){

		for (int i = 0; i < tileSize*tileSize; i++){

			if (unfiltredResults[i * 32 + 31] > 1){
				cout << i << endl;
				for (int j = 0; j<32; j++){
					if (unfiltredResults[i * 32 + j] > 0){
						file << setfill('0') << setw(16) << hex << unfiltredResults[i * 32 + j];
					}
				}
				file << dec << " " << xOffset*tileSize + i%tileSize << " " << yOffset*tileSize + i / tileSize;
				file << "\n";
			}
		}
		file.close();
	}
}

void allocateMemoryForKeys(uint64 **c_keysX, uint64 **c_keysY, int tileSize){

	CUDA_CHECK(cudaMalloc((void**)c_keysX, tileSize*keySizeInBytes));
	CUDA_CHECK(cudaMalloc((void**)c_keysY, tileSize*keySizeInBytes));
}

void allocateMemoryForTempKeys(uint64 **c_tempX, uint64 **c_tempY, int tileSize){

	CUDA_CHECK(cudaMalloc((void**)c_tempX, tileSize*tileSize*keySizeInBytes));
	CUDA_CHECK(cudaMalloc((void**)c_tempY, tileSize*tileSize*keySizeInBytes));
}

void computeGCD(dim3 threads, dim3 blocks, int tileSize, string inputFile, string outputFile){

	CUDA_CHECK(cudaSetDevice(0));
	CUDA_CHECK(cudaDeviceReset());

	struct cudaDeviceProp properties;
	cudaGetDeviceProperties(&properties, 0);

	uint64 * keys;
	int keysCount = getFileLinesCount(inputFile);
	cudaHostAlloc((void**)&keys, keysCount*keySizeInBytes, 0);
	memset(keys, 0, keysCount*keySizeInBytes);
	loadKeysFromFile(inputFile, keys);

	uint64 * unfiltredResults = new uint64[tileSize*tileSize*keySizeInBytes];
	memset(unfiltredResults, 0, tileSize*tileSize*keySizeInBytes);

	uint64 *c_keysX = 0;
	uint64 *c_keysY = 0;
	allocateMemoryForKeys(&c_keysX, &c_keysY, tileSize);

	uint64 *c_tempX = 0;
	uint64 *c_tempY = 0;
	allocateMemoryForTempKeys(&c_tempX, &c_tempY, tileSize);


	int iteartions = ceil(keysCount / tileSize);
	for (int yOffset = 0; yOffset < iteartions; yOffset++){
		for (int xOffset = 0; xOffset < iteartions; xOffset++){

			if (yOffset <= xOffset){

				CUDA_CHECK(cudaMemset(c_tempX, 0, tileSize*tileSize*keySizeInBytes));
				CUDA_CHECK(cudaMemset(c_tempY, 0, tileSize*tileSize*keySizeInBytes));

				CUDA_CHECK(cudaMemcpyAsync(c_keysX, keys + xOffset*tileSize * 32, tileSize*keySizeInBytes, cudaMemcpyHostToDevice, 0));
				CUDA_CHECK(cudaMemcpyAsync(c_keysY, keys + yOffset*tileSize * 32, tileSize*keySizeInBytes, cudaMemcpyHostToDevice, 0));

				GCD << <blocks, threads >> >(c_keysX, c_keysY, c_tempX, c_tempY, xOffset, yOffset, tileSize);

				cudaDeviceSynchronize();
				CUDA_CHECK(cudaGetLastError());
				CUDA_CHECK(cudaMemcpyAsync(unfiltredResults, c_tempX, tileSize*tileSize*keySizeInBytes, cudaMemcpyDeviceToHost, 0));
				saveResultsToFile(unfiltredResults, tileSize, outputFile, xOffset, yOffset);
			}
		}
	}
}

int main(int argc, char *argv[]){

	string inputFile = argv[1];
	string outputFile = argv[2];
	ofstream outfile(outputFile);

	int tileSize = 256;
	dim3 threads(16, 16);
	dim3 blocks(16, 16);
	computeGCD(threads, blocks, tileSize, inputFile, outputFile);

	cout << "COMPUTE END\n";
	return 0;
}